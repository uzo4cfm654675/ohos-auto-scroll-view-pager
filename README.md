# ohos-auto-scroll-view-pager

#### 项目介绍
- 项目名称：ohos-auto-scroll-view-pager
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个可设置间隔的自动滑动pagerslider组件
- 项目移植状态：主功能完成，由于原库版本太低，ohos组件移植的是新组件，新组件在废弃库的readme里面有说明，新组件是一个全新的库，所以完全可以直接看新组件库就可以。
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v1.1.2

#### 效果演示
<img src="auto-scroll.gif"></img>

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:ohos-auto-scroll-view-pager:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

在布局中引入自定义组件，例如：
```示例XML
    <pl.pzienowicz.autoscrollviewpager.AutoScrollViewPager
        ohos:id="$+id:autoviewpager"
        ohos:height="match_parent"
        ohos:width="match_parent"/>
```
JAVA
```java
        viewPager = (AutoScrollViewPager) findComponentById(ResourceTable.Id_autoviewpager);
        viewPager.setProvider(imagesAdapter);
        viewPager.setInterval(2000L);
        viewPager.setDirection(AutoScrollViewPager.Direction.RIGHT);
        viewPager.setCycle(true);
        viewPager.setBorderAnimation(true);
        viewPager.startAutoScroll();
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息
    Copyright 2014 trinea.cn
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
       http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.



