package pl.pzienowicz.autoscrollviewpager;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.PageSlider;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.multimodalinput.event.TouchEvent;

public class AutoScrollViewPager extends PageSlider {
    /**
     * 滑动方向
     */
    public enum Direction { LEFT, RIGHT }

    private static final int SCROLL_WHAT = 0;
    private Direction direction = Direction.RIGHT;
    private EventHandler eventHandler;
    private boolean isCycle = true;
    private boolean isBorderAnimation = true;
    private long interval = 1000L;
    private boolean isAutoScroll = false;
    private boolean isStopByTouch = false;

    private TouchEventListener listener = new TouchEventListener() {
        @Override
        public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
            if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN && isAutoScroll) {
                isStopByTouch = true;
                stopAutoScroll();
            } else if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP && isStopByTouch) {
                startAutoScroll();
            }
            return true;
        }
    };

    /**
     * constructor
     *
     * @param context context
     */
    public AutoScrollViewPager(Context context) {
        super(context);
        init();
    }

    /**
     * constructor
     *
     * @param context context
     * @param attrSet attrSet
     */
    public AutoScrollViewPager(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    /**
     * constructor
     *
     * @param context   context
     * @param attrSet   attrSet
     * @param styleName styleName
     */
    public AutoScrollViewPager(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        eventHandler = new MyHandler(EventRunner.getMainEventRunner());
        setOrientation(DirectionalLayout.HORIZONTAL);
        setTouchEventListener(listener);
    }

    private class MyHandler extends EventHandler {
        MyHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (event == null) {
                return;
            }
            int eventId = event.eventId;
            switch (eventId) {
                case SCROLL_WHAT:
                    scrollOnce();
                    sendEvent(SCROLL_WHAT, interval);
                    break;
                default:
                    break;
            }
        }
    }

    private void scrollOnce() {
        if (getProvider() == null || getProvider().getCount() <= 1) {
            return;
        }
        int currentItem = getCurrentPage();
        int totalCount = getProvider().getCount();
        int nextItem;
        if (direction == Direction.LEFT) {
            nextItem = --currentItem;
        } else {
            nextItem = ++currentItem;
        }
        if (nextItem < 0) {
            if (isCycle) {
                setCurrentPage(totalCount - 1, isBorderAnimation);
            }
        } else if (nextItem == totalCount) {
            if (isCycle) {
                setCurrentPage(0, isBorderAnimation);
            }
        } else {
            setCurrentPage(nextItem, true);
        }
    }

    /**
     * getDirection
     *
     * @return Direction
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * setDirection
     *
     * @param direction direction
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * startAutoScroll
     */
    public void startAutoScroll() {
        isAutoScroll = true;
        isStopByTouch = false;
        eventHandler.sendEvent(SCROLL_WHAT, interval);
    }

    /**
     * stopAutoScroll
     */
    public void stopAutoScroll() {
        isAutoScroll = false;
        eventHandler.removeEvent(SCROLL_WHAT);
    }

    /**
     * getInterval
     *
     * @return interval
     */
    public long getInterval() {
        return interval;
    }

    /**
     * setInterval
     *
     * @param interval interval
     */
    public void setInterval(long interval) {
        this.interval = interval;
    }

    /**
     * isCycle
     *
     * @return isCycle
     */
    public boolean isCycle() {
        return isCycle;
    }

    /**
     * setCycle
     *
     * @param cycle cycle
     */
    public void setCycle(boolean cycle) {
        isCycle = cycle;
    }

    /**
     * isBorderAnimation
     *
     * @return isBorderAnimation
     */
    public boolean isBorderAnimation() {
        return isBorderAnimation;
    }

    /**
     * setBorderAnimation
     *
     * @param borderAnimation borderAnimation
     */
    public void setBorderAnimation(boolean borderAnimation) {
        isBorderAnimation = borderAnimation;
    }
}
