package pl.pzienowicz.autoscrollviewpager.demo.slice;

import ohos.agp.components.*;
import ohos.app.Context;
import pl.pzienowicz.autoscrollviewpager.demo.ResourceTable;

import java.util.Arrays;

public class ImagesAdapter extends PageSliderProvider {
    private int[] images;
    private Context mContext;

    /**
     * constructor
     *
     * @param context context
     */
    public ImagesAdapter(Context context) {
        mContext = context;
    }

    public int[] getImages() {
        return images.clone();
    }

    public void setImages(int[] images) {
        this.images = Arrays.copyOf(images, images.length);
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        DirectionalLayout rootLayout = (DirectionalLayout) LayoutScatter.getInstance(mContext)
                .parse(ResourceTable.Layout_pagerslide_item, null, false);
        Image image = (Image) rootLayout.findComponentById(ResourceTable.Id_item_image);
        image.setPixelMap(images[i]);
        componentContainer.addComponent(rootLayout);
        return rootLayout;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        DirectionalLayout view = (DirectionalLayout) o;
        componentContainer.removeComponent(view);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component.equals(o);
    }
}
