package pl.pzienowicz.autoscrollviewpager.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import pl.pzienowicz.autoscrollviewpager.demo.slice.MainAbilitySlice;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
