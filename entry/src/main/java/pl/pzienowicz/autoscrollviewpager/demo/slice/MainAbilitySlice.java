package pl.pzienowicz.autoscrollviewpager.demo.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import pl.pzienowicz.autoscrollviewpager.AutoScrollViewPager;
import pl.pzienowicz.autoscrollviewpager.demo.ResourceTable;

public class MainAbilitySlice extends AbilitySlice {
    private ImagesAdapter imagesAdapter;
    private AutoScrollViewPager viewPager;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initDatas();
        initViews();
    }

    private void initDatas() {
        imagesAdapter = new ImagesAdapter(this);
        int[] list = new int[4];
        list[0] = ResourceTable.Media_1;
        list[1] = ResourceTable.Media_2;
        list[2] = ResourceTable.Media_3;
        list[3] = ResourceTable.Media_4;
        imagesAdapter.setImages(list);
    }

    private void initViews() {
        viewPager = (AutoScrollViewPager) findComponentById(ResourceTable.Id_autoviewpager);
        viewPager.setProvider(imagesAdapter);
        viewPager.setInterval(2000L);
        viewPager.setDirection(AutoScrollViewPager.Direction.RIGHT);
        viewPager.setCycle(true);
        viewPager.setBorderAnimation(true);
        viewPager.startAutoScroll();
    }
}
