package pl.pzienowicz.autoscrollviewpager.demo;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    // UI组件，不涉及单元测试
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("pl.pzienowicz.autoscrollviewpager.demo", actualBundleName);
    }
}